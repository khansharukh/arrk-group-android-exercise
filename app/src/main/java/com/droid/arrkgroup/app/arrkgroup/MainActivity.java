package com.droid.arrkgroup.app.arrkgroup;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.droid.arrkgroup.app.arrkgroup.Adapter.CharactersAdapter;
import com.droid.arrkgroup.app.arrkgroup.Listener.RecyclerTouchListener;
import com.droid.arrkgroup.app.arrkgroup.Utilities.Utilities;
import com.droid.arrkgroup.app.arrkgroup.Utils.Characters;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView cList;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private List<Characters> characterList;
    private RecyclerView.Adapter adapter;
    private TextView tv_errorText;
    private Button btn_errorBtn;
    private ConstraintLayout cl_rev, cl_net;
    public ProgressDialog loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cl_rev = (ConstraintLayout) findViewById(R.id.cl_revContainer);
        cl_net = (ConstraintLayout) findViewById(R.id.cl_netContainer);
        tv_errorText = (TextView) findViewById(R.id.tv_errorState);
        btn_errorBtn = (Button) findViewById(R.id.btn_errorBtn);

        cList = findViewById(R.id.rv_character);

        characterList = new ArrayList<>();
        adapter = new CharactersAdapter(getApplicationContext(), characterList);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(cList.getContext(), linearLayoutManager.getOrientation());

        cList.setHasFixedSize(true);
        cList.setLayoutManager(linearLayoutManager);
        cList.addItemDecoration(dividerItemDecoration);
        cList.setAdapter(adapter);

        cList.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), cList, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Characters character = characterList.get(position);
                String charUrl = character.getCharLink();

                Intent intent = new Intent(MainActivity.this, CharacterActivity.class);
                intent.putExtra("char_url", charUrl);
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        getUserCharacters();
    }
    public void getUserCharacters() {
        cl_rev.setVisibility(View.VISIBLE);
        loading = ProgressDialog.show(MainActivity.this, "Fetching star wars characters", "Please wait...", false, false);
        if(!isNetworkAvailable()) {
            loading.dismiss();
            updateUI("No internet connection", "Try Again");
        }
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Utilities.DATA_URL+"people",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loading.dismiss();
                        JSONObject result = null;
                        try {
                            result = new JSONObject(response);
                            String data = result.getString("results");
                            if(!data.isEmpty()) {
                                JSONArray dataArray = new JSONArray(data);

                                for (int i = 0; i < dataArray.length(); i++) {
                                    try {
                                        JSONObject jsonObject = dataArray.getJSONObject(i);
                                        String charName = jsonObject.getString("name");
                                        String charLink = jsonObject.getString("url");

                                        Characters character = new Characters();
                                        character.setCharName(charName);
                                        character.setCharLink(charLink);

                                        characterList.add(character);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                adapter.notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(isNetworkAvailable()) {
                    updateUI("Error fetching values, please retry", "Retry");
                }
            }
        });
        //Creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        //Adding request to the queue
        requestQueue.add(stringRequest);
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void retryMethod(View view) {
        getUserCharacters();
    }

    public void updateUI(String errorText, String btnText) {
        tv_errorText.setText(errorText);
        btn_errorBtn.setText(btnText);
        cl_rev.setVisibility(View.GONE);
        cl_net.setVisibility(View.VISIBLE);
    }
}
