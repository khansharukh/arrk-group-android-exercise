package com.droid.arrkgroup.app.arrkgroup;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.droid.arrkgroup.app.arrkgroup.Utilities.Utilities;
import com.droid.arrkgroup.app.arrkgroup.Utils.Characters;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CharacterActivity extends AppCompatActivity {
    public String url;
    public ProgressDialog loading;
    private TextView tv_name, tv_mass, tv_height, tv_created;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        url = intent.getStringExtra("char_url");

        tv_name = (TextView) findViewById(R.id.tv_charName);
        tv_height = (TextView) findViewById(R.id.tv_height);
        tv_mass = (TextView) findViewById(R.id.tv_mass);
        tv_created = (TextView) findViewById(R.id.tv_created);

        getCharacterDetails(url);
    }

    private void getCharacterDetails(String url) {
        loading = ProgressDialog.show(CharacterActivity.this, "Fetching character details", "Please wait...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loading.dismiss();
                        JSONObject result = null;
                        try {
                            result = new JSONObject(response);
                            String name = result.getString("name");
                            String height = result.getString("height");
                            String mass = result.getString("mass");
                            String created_at = result.getString("created");

                            Date d = null;
                            try {
                                d = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(created_at);
                            }
                            catch (ParseException e) {
                                e.printStackTrace();
                            }
                            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm a");

                            tv_name.setText(name);
                            tv_height.setText(height);
                            tv_mass.setText(mass + " kg");
                            tv_created.setText(sdf.format(d));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        //Creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(CharacterActivity.this);
        //Adding request to the queue
        requestQueue.add(stringRequest);
    }
}
