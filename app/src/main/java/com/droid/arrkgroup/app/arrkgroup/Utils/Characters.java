package com.droid.arrkgroup.app.arrkgroup.Utils;

public class Characters {
    private String charName, charLink;

    public Characters() {

    }

    public Characters(String charName) {
        this.charName = charName;
    }

    public String getCharName() {
        return charName;
    }

    public void setCharName(String charName) {
        this.charName = charName;
    }

    public String getCharLink() {
        return charLink;
    }

    public void setCharLink(String charLink) {
        this.charLink = charLink;
    }
}
