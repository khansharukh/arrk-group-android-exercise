package com.droid.arrkgroup.app.arrkgroup.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.droid.arrkgroup.app.arrkgroup.R;
import com.droid.arrkgroup.app.arrkgroup.Utils.Characters;

import java.util.List;

public class CharactersAdapter extends RecyclerView.Adapter<CharactersAdapter.ViewHolder> {
    private Context context;
    private List<Characters> list;

    public CharactersAdapter(Context context, List<Characters> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public CharactersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.single_character, parent, false);
        return new CharactersAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CharactersAdapter.ViewHolder holder, int position) {
        Characters character = list.get(position);

        holder.tv_characterTitle.setText(character.getCharName());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_characterTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_characterTitle = itemView.findViewById(R.id.tv_characterName);
        }
    }
}